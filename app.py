import json
import os
import logging
import logging.config
import connexion
from connexion import NoContent
from swagger_ui_bundle import swagger_ui_path
import requests
import yaml
from pykafka import KafkaClient
from flask_cors import CORS, cross_origin

# Load configuration files
with open('app_conf.yaml', 'r', encoding='utf-8') as f:
    app_config = yaml.safe_load(f.read())

with open('log_conf.yml', 'r', encoding='utf-8') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

def get_deposit_reading(index):
    """ Get deposit Reading in History """
    hostname = f"{app_config['events']['hostname']}:{app_config['events']['port']}"
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config['events']['topic'])]
    consumer = topic.get_simple_consumer(reset_offset_on_start=True,
                                         consumer_timeout_ms=1000)
    logger.info(f"Retrieving deposit at index {index}")
    try:
        lst = []
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)
            logger.debug(msg['type'])
            if msg['type'] == 'deposit':
                lst.append(msg)
        logger.debug(lst[index])
        return lst[index], 201

    except IndexError:
        logger.error("No more messages found")
    logger.error(f"Could not find deposit at index {index}")
    return {"message": "Not Found"}, 404


def get_withdrawal_readings(index):
    """ Get Withdrawal Reading in History """
    hostname = f"{app_config['events']['hostname']}:{app_config['events']['port']}"
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config['events']['topic'])]
    consumer = topic.get_simple_consumer(reset_offset_on_start=True,
                                         consumer_timeout_ms=1000)
    logger.info(f"Retrieving withdrawal at index {index}")
    try:
        lst = []
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)
            logger.debug(msg)
            if msg['type'] == 'withdrawal':
                lst.append(msg)
        return lst[index], 201

    except IndexError:
        logger.error("No more messages found")
    logger.error(f"Could not find withdrawal at index {index}")
    return {"message": "Not Found"}, 404

# Flask App Configuration
app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'
app.add_api("openapi.yaml")

if __name__ == "__main__":
    app.run(port=8110)
